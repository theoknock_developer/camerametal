//
//  DispatchTest.m
//  DispatchSourceQueueEventHandlerSampleCode
//
//  Created by Xcode Developer on 5/30/21.
//

#import "TextureSourceQueueEvent.h"
#import <Metal/Metal.h>

@implementation TextureSourceQueueEvent

static TextureSourceQueueEvent * sharedDispatchTest = NULL;
+ (nonnull TextureSourceQueueEvent *)textureSourceQueueEvent
{
    if (!sharedDispatchTest)
    {
        sharedDispatchTest = [[self alloc] init];
    }
    
    return sharedDispatchTest;
}

- (instancetype)init
{
    self = [super init];
    if (self != nil)
    {
        self.event_index = 1;
//        dispatch_queue_t dispatch_queue =  [self  textureDispatchQueue];
//        dispatch_source_t dispatch_source = [self textureDispatchSource];
        
        
    }
    
    return self;
}


//- (void)testDispatch
//{
//   dispatch_async(self->_textureDispatchQueue, ^{
//        do {
//            [self textureDispatchSource];
//            struct DispatchSourceObjectInt * dispatch_source_object_int_ptr_1 = randomInt();
//
//            NSLog(@"Setting index = %ld\t\tRandom integer = %d", self->_event_index, dispatch_source_object_int_ptr_1->rnd_int);
//            const char *label = [[NSString stringWithFormat:@"%ld", self->_event_index] cStringUsingEncoding:NSUTF8StringEncoding];
//            dispatch_queue_set_specific(self->_textureDispatchQueue, label, dispatch_source_object_int_ptr_1, NULL);
//            dispatch_source_merge_data(self->_textureDispatchSource, self->_event_index);
//
//            self->_textureDispatchSource = nil;
//            self->_event_index++;
//        } while (self->_event_index < 11);
//    });
//}

//- (dispatch_queue_t)textureDispatchQueue
//{
//    __block dispatch_queue_t q = self->_textureDispatchQueue;
//    if (!q)
//    {
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            q = dispatch_queue_create_with_target("_textureQueue", DISPATCH_QUEUE_CONCURRENT, dispatch_get_main_queue());
//            self->_textureDispatchQueue = q;
//        });
//    }
//    
//    return q;
//}

//- (dispatch_source_t)textureDispatchSource
//{
//    dispatch_queue_t dispatch_queue = self->_textureDispatchQueue;
//    dispatch_source_t dispatch_source = self->_textureDispatchSource;
//    if (!dispatch_source)
//    {
//        dispatch_source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_queue);
//        dispatch_source_set_event_handler(dispatch_source, ^{
//            dispatch_async(dispatch_queue, ^{
//                NSInteger index = dispatch_source_get_data(dispatch_source);
//                PixelBufferData *context = (PixelBufferData *)malloc(sizeof(PixelBufferData));
//                if (context != NULL)
//                {
//                    context = (PixelBufferData *)dispatch_queue_get_specific(dispatch_queue, [[NSString stringWithFormat:@"%lu", index] cStringUsingEncoding:NSUTF8StringEncoding]);
////                    id<MTLTexture> texture = (id<MTLTexture>)CFBridgingRelease(context->texture);
//                    MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];
//                    textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;
//                    textureDescriptor.width = image.width;
//                    textureDescriptor.height = image.height;
//                    id<MTLTexture> texture = [_device newTextureWithDescriptor:textureDescriptor];
//                }
//                
//                
//            });
//        });
//        dispatch_set_target_queue(dispatch_source, dispatch_queue);
//        
//        
//        self->_textureDispatchSource = dispatch_source;
//        dispatch_resume(self->_textureDispatchSource);
//    }
//    
//    return self->_textureDispatchSource;
//}


@end

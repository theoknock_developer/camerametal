 //
//  MetalContext.m
//  Obscura
//
//  Created by James Bush on 5/29/18.
//  Copyright © 2018 James Bush. All rights reserved.
//

#import "MetalContext.h"
#import "MBEMatrixUtilities.h"
#import "AAPLTransforms.h"

#if !defined(_STRINGIFY)
#define __STRINGIFY( _x )   # _x
#define _STRINGIFY( _x )   __STRINGIFY( _x )
#endif

float quadVertexData[16] =
{
    -1.0, -1.0,  0.0, 1.0,
    1.0, -1.0,  1.0, 1.0,
    -1.0,  1.0,  0.0, 0.0,
    1.0,  1.0,  1.0, 0.0,
};

@implementation MetalContexts

- (CGSize)viewSize
{
    if([self.delegate respondsToSelector:@selector(viewDrawableSize)])
        return [self.delegate viewDrawableSize];
    else
        return CGSizeMake(2160.0, 3840.0);
}

NSString *(^generateServerName)(void) = ^(void)
{
    CMTime timestamp = CMClockGetTime(CMClockGetHostTimeClock());
    return [NSString stringWithFormat:@"%@+%f!%@", [[UIDevice currentDevice] name], CMTimeGetSeconds(timestamp), [[[UIDevice currentDevice] identifierForVendor] UUIDString]];
};

typedef NSString *(^StringifyArrayOfIncludes)(NSArray <NSString *> *includes);
static NSString *(^stringifyHeaderFileNamesArray)(NSArray <NSString *> *) = ^(NSArray <NSString *> *includes) {
    NSMutableString *importStatements = [NSMutableString new];
    [includes enumerateObjectsUsingBlock:^(NSString * _Nonnull include, NSUInteger idx, BOOL * _Nonnull stop) {
        [importStatements appendString:@"#include <"];
        [importStatements appendString:include];
        [importStatements appendString:@">\n"];
    }];
    
    return [NSString new];
};

typedef NSString *(^StringifyArrayOfHeaderFileNames)(NSArray <NSString *> *headerFileNames);
static NSString *(^stringifyIncludesArray)(NSArray *) = ^(NSArray *headerFileNames) {
    NSMutableString *importStatements = [NSMutableString new];
    [headerFileNames enumerateObjectsUsingBlock:^(NSString * _Nonnull headerFileName, NSUInteger idx, BOOL * _Nonnull stop) {
        [importStatements appendString:@"#import "];
        [importStatements appendString:@_STRINGIFY("")];
        [importStatements appendString:headerFileName];
        [importStatements appendString:@_STRINGIFY("")];
        [importStatements appendString:@"\n"];
    }];
    
    return [NSString new];
};

- (NSString *)shader
{
    NSString *includes = stringifyIncludesArray(@[@"metal_stdlib", @"simd/simd.h"]);
//    NSString *imports  = stringifyHeaderFileNamesArray(@[@"ShaderTypes.h"]);
    NSString *code     = [NSString stringWithFormat:@"%s",
                          _STRINGIFY(
                                     using namespace metal;
                                     
                                     typedef struct {
                                         float scale_factor;
                                         float display_configuration;
                                         float view_width;
                                         float view_height;
                                     } Uniforms;
                                     
                                     typedef struct {
                                         float4 renderedCoordinate [[position]];
                                         float2 textureCoordinate;
                                     } TextureMappingVertex;
                                     
                                     vertex TextureMappingVertex mapTexture(unsigned int vertex_id [[ vertex_id ]],
                                                                            constant Uniforms &uniform [[ buffer(1) ]])
                                    {
                                        float4x4 renderedCoordinates;
                                        float4x2 textureCoordinates;
                                        
                                        renderedCoordinates = float4x4(float4( -1.0, -1.0, 0.0, 1.0 ),
                                                                       float4(  1.0, -1.0, 0.0, 1.0 ),
                                                                       float4( -1.0,  1.0, 0.0, 1.0 ),
                                                                       float4(  1.0,  1.0, 0.0, 1.0 ));
                                        
                                        textureCoordinates = float4x2(float2( -1.0, 1.0 ),
                                                                      float2(  1.0, 1.0 ),
                                                                      float2( -1.0, 0.0 ),
                                                                      float2(  1.0, 0.0 ));
                                        
                                        TextureMappingVertex outVertex;
                                         /* renderedCoordinates[vertex_id]*/
                                        
//                                        // v’ = (v-min)/(max-min) * (newmax-newmin) + newmin
                                        float max = (uniform.view_width > uniform.view_height) ? 3840.0 : 2160.0;
                                        float normalized_x = ((renderedCoordinates[vertex_id].x - 0.0)/max) * ((uniform.view_width * uniform.scale_factor) - 0.0) + 0.0;
                                        float normalized_y = ((renderedCoordinates[vertex_id].y - 0.0)/max) * ((uniform.view_height * uniform.scale_factor) - 0.0) + 0.0;
                                        outVertex.renderedCoordinate = float4(normalized_x, normalized_y, 1.0f, 1.0f);
                                        outVertex.textureCoordinate = textureCoordinates[vertex_id];
                                        
                                        return outVertex;
                                    }

                                     
                                     fragment half4 displayTexture(TextureMappingVertex mappingVertex [[ stage_in ]],
                                                                   texture2d<float, access::sample> texture [[ texture(0) ]],
                                                                   sampler samplr [[sampler(0)]],
                                                                   constant Uniforms &uniform [[ buffer(1) ]]) {
                                         
                                         if (uniform.display_configuration == 1 ||
                                             uniform.display_configuration == 2 ||
                                             uniform.display_configuration == 4 ||
                                             uniform.display_configuration == 6 ||
                                             uniform.display_configuration == 7)
                                         {
                                             mappingVertex.textureCoordinate.x = 1 - mappingVertex.textureCoordinate.x;
                                         }
                                         
                                         if (uniform.display_configuration == 2 ||
                                             uniform.display_configuration == 6)
                                         {
                                             mappingVertex.textureCoordinate.y = 1 - mappingVertex.textureCoordinate.y;
                                         }
                                         
                                         if (uniform.display_configuration == 1 ||
                                             uniform.display_configuration == 3 ||
                                             uniform.display_configuration == 5 ||
                                             uniform.display_configuration == 7)
                                         {
                                             mappingVertex.textureCoordinate = float2(mappingVertex.textureCoordinate.y, mappingVertex.textureCoordinate.x);
                                         }
                                        
                                         // v’ = (v-min)/(max-min) * (newmax-newmin) + newmin
                                         
                                         half4 new_texture = half4(texture.sample(samplr, mappingVertex.textureCoordinate));
                                         
                                         return new_texture; //half4(new_texture.r, new_texture.g, new_texture.b, new_alpha);
                                     }

                                     
                                     )];
    
    return [NSString stringWithFormat:@"%@\n%@", includes, code];
}

- (instancetype)init
{
    if ((self = [super init]))
    {
        _device = MTLCreateSystemDefaultDevice();
        
        __autoreleasing NSError *error = nil;
        NSString* librarySrc = [self shader];
        if(!librarySrc) {
            [NSException raise:@"Failed to read shaders" format:@"%@", [error localizedDescription]];
        }

        _library = [_device newLibraryWithSource:librarySrc options:nil error:&error];
        if(!_library) {
            [NSException raise:@"Failed to compile shaders" format:@"%@", [error localizedDescription]];
        }
        
        id <MTLFunction> vertexProgram = [_library newFunctionWithName:@"mapTexture"];
        id <MTLFunction> fragmentProgram = [_library newFunctionWithName:@"displayTexture"];
        
        // Describe and create a render pipeline state
        MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
        pipelineStateDescriptor.label = @"Fullscreen Quad Pipeline";
        pipelineStateDescriptor.vertexFunction = vertexProgram;
        pipelineStateDescriptor.fragmentFunction = fragmentProgram;
        pipelineStateDescriptor.vertexDescriptor = [self vertexDescriptor];
        pipelineStateDescriptor.colorAttachments[0].pixelFormat = MTLPixelFormatBGRA8Unorm_sRGB;
        pipelineStateDescriptor.depthAttachmentPixelFormat = MTLPixelFormatDepth32Float_Stencil8;
        pipelineStateDescriptor.stencilAttachmentPixelFormat = MTLPixelFormatDepth32Float_Stencil8;
        pipelineStateDescriptor.sampleCount = 1;
        
        _renderPipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
        if (!_renderPipelineState)
        {
            NSLog(@"Failed to create render pipeline state, error %@", error);
        }
        
        [self buildSamplerState];
        [self buildResources];
    
        _commandQueue = [_device newCommandQueue];
        [_commandQueue setLabel:generateServerName()];
    }
    
    return self;
}

// ignore direct setting of tAddressMode for now
- (void)buildSamplerState
{
    MTLSamplerDescriptor *samplerDescriptor = [MTLSamplerDescriptor new];
    samplerDescriptor.sAddressMode = MTLSamplerAddressModeMirrorRepeat;
    if ([[DisplayConfigurationManager sharedDisplayConfigurationManager] globalConfiguration] == 1)
        samplerDescriptor.tAddressMode = MTLSamplerAddressModeClampToZero;
    else
        samplerDescriptor.tAddressMode = MTLSamplerAddressModeMirrorRepeat;
    samplerDescriptor.rAddressMode = MTLSamplerAddressModeClampToZero;
    samplerDescriptor.minFilter = MTLSamplerMinMagFilterNearest;
    samplerDescriptor.magFilter = MTLSamplerMinMagFilterNearest;
    samplerDescriptor.normalizedCoordinates = YES;
    _samplerState = [_device newSamplerStateWithDescriptor:samplerDescriptor];
}

- (void)buildResources
{
    MTLDepthStencilDescriptor *pDepthStateDesc = [MTLDepthStencilDescriptor new];
    pDepthStateDesc.depthCompareFunction = MTLCompareFunctionLess;
    pDepthStateDesc.depthWriteEnabled    = YES;
    _depthState = [_device newDepthStencilStateWithDescriptor:pDepthStateDesc];

    _vertexBuffer = [_device newBufferWithBytes:quadVertexData length:sizeof(quadVertexData) options:MTLResourceOptionCPUCacheModeWriteCombined];
    _vertexBuffer.label = @"Vertices";

    _uniformBuffer = [_device newBufferWithLength:sizeof(Uniforms)
                                          options:MTLResourceOptionCPUCacheModeWriteCombined];
    
    [_uniformBuffer setLabel:@"Uniforms"];
    
    [self setDisplayConfiguration:PortraitUp scale:0.15];
}

- (MTLVertexDescriptor *)vertexDescriptor
{
    MTLVertexDescriptor *vertexDescriptor = [MTLVertexDescriptor vertexDescriptor];
    
    vertexDescriptor.attributes[0].format = MTLVertexFormatFloat2; // position
    vertexDescriptor.attributes[0].offset = 0;
    vertexDescriptor.attributes[0].bufferIndex = 0;
    
    vertexDescriptor.attributes[1].format = MTLVertexFormatFloat2; // texCoords
    vertexDescriptor.attributes[1].offset = 2 * sizeof(float);
    vertexDescriptor.attributes[1].bufferIndex = 0;
    
    vertexDescriptor.layouts[0].stepRate = 1;
    vertexDescriptor.layouts[0].stepFunction = MTLVertexStepFunctionPerVertex;
    vertexDescriptor.layouts[0].stride = 4 * sizeof(float);
    
//    MTLVertexDescriptor *descriptor = [MTLVertexDescriptor vertexDescriptor];
//    descriptor.attributes[0].format = MTLVertexFormatFloat4;
//    descriptor.attributes[0].offset = 0;
//    descriptor.attributes[0].bufferIndex = 0;
//
//    descriptor.attributes[1].format = MTLVertexFormatFloat2;
//    descriptor.attributes[1].offset = offsetof(MBEVertex, texCoords);
//    descriptor.attributes[1].bufferIndex = 0;
//
//    descriptor.layouts[0].stride = MBEUniformBufferLength;
    
    return vertexDescriptor;
}

- (void)writeUniformBuffer
{
    [self buildSamplerState];
    
    Uniforms uniforms;
    uniforms.scale_factor = [[DisplayConfigurationManager sharedDisplayConfigurationManager] zoomScale];
    uniforms.display_configuration = (NSInteger)[[DisplayConfigurationManager sharedDisplayConfigurationManager] globalConfiguration];
    uniforms.view_width  = (float)[self viewSize].width;
    uniforms.view_height = (float)[self viewSize].height;
    _uniformBuffer = [_device newBufferWithLength:sizeof(Uniforms)
                                          options:MTLResourceOptionCPUCacheModeWriteCombined];
    void *bufferPointer = [_uniformBuffer contents];
    
    memcpy(bufferPointer, &uniforms, sizeof(Uniforms));
}

- (void)updateScaleFactor:(CGFloat)scaleFactor;
{
    CGFloat currentScaleFactor = [[DisplayConfigurationManager sharedDisplayConfigurationManager] zoomScale];
//    if (fabs(currentScaleFactor - scaleFactor) > 0.001)
//    {
        [[DisplayConfigurationManager sharedDisplayConfigurationManager] setZoomScale:scaleFactor];
    
//    NSLog(@"Scale %f", [[DisplayConfigurationManager sharedDisplayConfigurationManager] zoomScale]);
        [self writeUniformBuffer];
//    }
}

- (NSInteger)updateDisplayConfiguration
{
    [[DisplayConfigurationManager sharedDisplayConfigurationManager] nextConfiguration];
    [self writeUniformBuffer];

    return (NSInteger)[[DisplayConfigurationManager sharedDisplayConfigurationManager] globalConfiguration];
}

- (void)setDisplayConfiguration:(Configuration)configuration scale:(CGFloat)scaleFactor
{
    [[DisplayConfigurationManager sharedDisplayConfigurationManager] setZoomScale:scaleFactor];
    [[DisplayConfigurationManager sharedDisplayConfigurationManager] setGlobalConfiguration:configuration];
    
    [self writeUniformBuffer];
}

@end

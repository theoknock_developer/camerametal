//
//  CameraTextureViewShader.h
//  Chroma06252018
//
//  Created by Xcode Developer on 6/26/18.
//  Copyright © 2018 Xcode Developer. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Shaders : NSObject

+ (Shaders *)sharedShaders;

@property (strong, nonatomic, readwrite) NSString *cameraTextureShader;

@end

NS_ASSUME_NONNULL_END

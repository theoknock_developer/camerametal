//
//  MTKViewController.h
//  Camera-Metal
//
//  Created by Xcode Developer on 5/31/21.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import <simd/simd.h>

#import "TextureSourceQueueEvent.h"


NS_ASSUME_NONNULL_BEGIN


//typedef struct {
//    matrix_float4x4 rotationMatrix;
//} Uniforms;
//
//typedef struct {
//    vector_float4 position;
//    vector_float4 color;
//} VertexIn;
//
//static const VertexIn vertexData[] =
//{
//    { { 0.5, -0.5, 0.0, 1.0}, {1.0, 0.0, 0.0, 1.0} },
//    { {-0.5, -0.5, 0.0, 1.0}, {0.0, 1.0, 0.0, 1.0} },
//    { {-0.5,  0.5, 0.0, 1.0}, {0.0, 0.0, 1.0, 1.0} },
//    { { 0.5,  0.5, 0.0, 1.0}, {1.0, 1.0, 0.0, 1.0} },
//    { { 0.5, -0.5, 0.0, 1.0}, {1.0, 0.0, 0.0, 1.0} },
//    { {-0.5,  0.5, 0.0, 1.0}, {0.0, 0.0, 1.0, 1.0} }
//};
//
//static matrix_float4x4 rotationMatrix2D(float radians)
//{
//    float cos = cosf(radians);
//    float sin = sinf(radians);
//    return (matrix_float4x4) {
//        .columns[0] = {  cos, sin, 0, 0 },
//        .columns[1] = { -sin, cos, 0, 0 },
//        .columns[2] = {    0,   0, 1, 0 },
//        .columns[3] = {    0,   0, 0, 1 }
//    };
//}

/*
 id<MTLTexture>(^CVPixelBufferCreateMTLTexture)(void) = ^(CVPixelBufferRef pixelBuffer) {
     return ^id<MTLTexture>() {
         return texture;
     }
 } (pixelBuffer);
 */

typedef id<MTLTexture>(^CVPixelBufferCreateMTLTexture)(CVPixelBufferRef pixel_buffer);

@protocol CMSampleBufferImageBufferDelegate;

@interface MTKViewController : UIViewController

@property (strong, nonatomic, nullable) __block dispatch_source_t CVPixelBufferDispatchSource;
@property (strong, nonatomic, nullable) __block dispatch_queue_t CVPixelBufferDispatchQueue;
@property (strong, nonatomic) __block TextureSourceQueueEvent *textureHandler;
@property (weak, nonatomic, setter=sampleBufferImageBufferDelegate:) id<CMSampleBufferImageBufferDelegate>sampleBufferImageBufferDelegate;


@end

@protocol CMSampleBufferImageBufferDelegate <NSObject>

@required

// To-Do: Make the CVPixelBufferCreateMTLTexture block a parameter to the render block,
//        which will be the only parameter to this method

- (void)drawTextureInMTKView:(MTKView *)mtkView withRenderer:(void(^)(CVPixelBufferCreateMTLTexture MTLTextureCreateFromCVPixelBuffer))renderTexture;

@end



NS_ASSUME_NONNULL_END

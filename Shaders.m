//
//  CameraTextureViewShader.m
//  Chroma06252018
//
//  Created by Xcode Developer on 6/26/18.
//  Copyright © 2018 Xcode Developer. All rights reserved.
//

#import "Shaders.h"

#if !defined(_STRINGIFY)
#define __STRINGIFY( _x )   # _x
#define _STRINGIFY( _x )   __STRINGIFY( _x )
#endif

@implementation Shaders

static Shaders *sharedShaders;
+ (Shaders *)sharedShaders
{
    if (sharedShaders == nil)
    {
        sharedShaders = [[self alloc] init];
        _cameraTextureShader = [self cameraTextureShader];
    }
    
    return sharedShaders;
}

static NSString * _cameraTextureShader;
+ (NSString *)cameraTextureShader { return _cameraTextureShader; }
+ (void)setCameraTextureShader:(NSString *)cameraTextureShader
{
    _cameraTextureShader = cameraTextureShader;
}

typedef NSString *(^StringifyArrayOfIncludes)(NSArray <NSString *> *includes);
static NSString *(^stringifyHeaderFileNamesArray)(NSArray <NSString *> *) = ^(NSArray <NSString *> *includes) {
    NSMutableString *importStatements = [NSMutableString new];
    [includes enumerateObjectsUsingBlock:^(NSString * _Nonnull include, NSUInteger idx, BOOL * _Nonnull stop) {
        [importStatements appendString:@"#include <"];
        [importStatements appendString:include];
        [importStatements appendString:@">\n"];
    }];
    
    return [NSString new];
};

typedef NSString *(^StringifyArrayOfHeaderFileNames)(NSArray <NSString *> *headerFileNames);
static NSString *(^stringifyIncludesArray)(NSArray *) = ^(NSArray *headerFileNames) {
    NSMutableString *importStatements = [NSMutableString new];
    [headerFileNames enumerateObjectsUsingBlock:^(NSString * _Nonnull headerFileName, NSUInteger idx, BOOL * _Nonnull stop) {
        [importStatements appendString:@"#import "];
        [importStatements appendString:@_STRINGIFY("")];
        [importStatements appendString:headerFileName];
        [importStatements appendString:@_STRINGIFY("")];
        [importStatements appendString:@"\n"];
    }];
    
    return [NSString new];
};

- (NSString *)cameraTextureShader
{
    NSString *includes = stringifyIncludesArray(@[@"metal_stdlib", @"simd/simd.h"]);
        NSString *imports  = stringifyHeaderFileNamesArray(@[@"ShaderTypes.h"]);
    NSString *code     = [NSString stringWithFormat:@"%s",
                          _STRINGIFY(
                                     using namespace metal;
                                     
                                     typedef struct {
                                         float2 scale_factor;
                                         float  display_configuration;
                                         float  view_width;
                                         float  view_height;
                                     } Uniforms;
                                     
                                     typedef struct {
                                         float4 renderedCoordinate [[position]];
                                         float2 textureCoordinate;
                                     } TextureMappingVertex;
                                     
                                     vertex TextureMappingVertex mapTexture(unsigned int vertex_id [[ vertex_id ]],
                                                                            constant Uniforms &uniform [[ buffer(1) ]])
                                     {
                                         float4x4 renderedCoordinates;
                                         float4x2 textureCoordinates;
                                         
                                         renderedCoordinates = float4x4(float4( -1.0, -1.0, 0.0, 1.0 ),
                                                                        float4(  1.0, -1.0, 0.0, 1.0 ),
                                                                        float4( -1.0,  1.0, 0.0, 1.0 ),
                                                                        float4(  1.0,  1.0, 0.0, 1.0 ));
                                         
                                         textureCoordinates = float4x2(float2( -1.0, 1.0 ),
                                                                       float2(  1.0, 1.0 ),
                                                                       float2( -1.0, 0.0 ),
                                                                       float2(  1.0, 0.0 ));
                                         
                                         TextureMappingVertex outVertex;
                                         /* renderedCoordinates[vertex_id]*/
                                         
                                         //                                        // v’ = (v-min)/(max-min) * (newmax-newmin) + newmin
//                                         float size_max = (uniform.view_width < uniform.view_height) ? uniform.view_height : uniform.view_width;
//                                         float normalized_x = ((renderedCoordinates[vertex_id].x)/size_max) * ((uniform.view_width * uniform.scale_factor.x));
//                                         float normalized_y = ((renderedCoordinates[vertex_id].y)/size_max) * ((uniform.view_width * (uniform.view_height/uniform.view_width)) * uniform.scale_factor.y);
                                         outVertex.renderedCoordinate = renderedCoordinates[vertex_id];// float4(normalized_x, normalized_y, uniform.scale_factor.x, uniform.scale_factor.y);
                                         outVertex.textureCoordinate = textureCoordinates[vertex_id];
                                         
                                         return outVertex;
                                     }
                                     
                                     
                                     fragment half4 displayTexture(TextureMappingVertex mappingVertex [[ stage_in ]],
                                                                   texture2d<float, access::sample> texture [[ texture(0) ]],
                                                                   sampler samplr [[sampler(0)]],
                                                                   constant Uniforms &uniform [[ buffer(1) ]]) {
                                         
                                         if (uniform == 1 ||
                                             uniform.display_configuration == 2 ||
                                             uniform.display_configuration == 4 ||
                                             uniform.display_configuration == 6 ||
                                             uniform.display_configuration == 7)
                                         {
                                             mappingVertex.textureCoordinate.x = 1 - mappingVertex.textureCoordinate.x;
                                         }
                                         
                                         if (uniform.display_configuration == 2 ||
                                             uniform.display_configuration == 6)
                                         {
                                             mappingVertex.textureCoordinate.y = 1 - mappingVertex.textureCoordinate.y;
                                         }
                                         
                                         if (uniform.display_configuration == 1 ||
                                             uniform.display_configuration == 3 ||
                                             uniform.display_configuration == 5 ||
                                             uniform.display_configuration == 7)
                                         {
                                             mappingVertex.textureCoordinate = float2(mappingVertex.textureCoordinate.y, mappingVertex.textureCoordinate.x);
                                         }
                                         
                                         // v’ = (v-min)/(max-min) * (newmax-newmin) + newmin
//                                         float size_max = (uniform.view_width < uniform.view_height) ? uniform.view_height : uniform.view_width;
//                                         float normalized_x = (mappingVertex.textureCoordinate.x/size_max) * (uniform.view_width * uniform.scale_factor.x);
//                                         float normalized_y = (mappingVertex.textureCoordinate.y/size_max) * (uniform.view_height * uniform.scale_factor.y);
                                         
                                         half4 new_texture = half4(texture.sample(samplr, mappingVertex.textureCoordinate));//float2(normalized_x, normalized_y)));
                                         
                                         // Blend with Screen //1 - (1 -a)(1 - b)//1 - (1 -a)(1 - b) Source: https://en.wikipedia.org/wiki/Blend_modes
                                         new_texture = half4(1 - ((1 - new_texture.r) * (1 - new_texture.r)),
                                                             1 - ((1 - new_texture.g) * (1 - new_texture.g)),
                                                             1 - ((1 - new_texture.b) * (1 - new_texture.b)),
                                                             1 - ((1 - new_texture.a) * (1 - new_texture.a)));
                                         
                                         
                                         return new_texture; //half4(new_texture.r, new_texture.g, new_texture.b, new_alpha);
                                     }
                                     
                                     
                                     )];
    
    return [NSString stringWithFormat:@"%@\n%@", includes, code];
}


@end

//
//  MetalContext.h
//  Obscura
//
//  Created by James Bush on 5/29/18.
//  Copyright © 2018 James Bush. All rights reserved.
//

#import "Foundation/Foundation.h"
#import "Metal/Metal.h"
#import "MetalKit/MetalKit.h"
#import "UIKit/UIKit.h"
#import <simd/simd.h>

@protocol MetalContextDelegate <NSObject>
@optional
- (CGSize)viewDrawableSize;
@end

typedef struct
{
    float scale_factor;
    float display_configuration;
    float view_width;
    float view_height;
} Uniforms; 


@interface MetalContext : NSObject

+ (MetalContext *)sharedMetalContext;
@property (nonatomic, weak) id<MetalContextDelegate> delegate;

@property (strong) id<MTLDevice> device;
@property (strong) id<MTLLibrary> library;
@property (strong) id<MTLCommandQueue> commandQueue;

@property (strong) MTLRenderPipelineDescriptor *pipelineDescriptor;
@property (nonatomic, strong) id <MTLRenderPipelineState> renderPipelineState;
@property (nonatomic, strong) id <MTLDepthStencilState> depthState;
@property (strong) id<MTLSamplerState> samplerState;

@property (nonatomic, strong) id<MTLBuffer> vertexBuffer;
@property (nonatomic, strong) id<MTLBuffer> uniformBuffer;

@property (nonatomic, assign) Uniforms uniforms;

- (void)updateScaleFactor:(CGFloat)scaleFactor;
- (NSInteger)updateDisplayConfiguration;
- (void)buildSamplerState;


@end

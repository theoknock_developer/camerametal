//
//  ViewController.h
//  Camera-Metal
//
//  Created by Xcode Developer on 5/29/21.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <MetalKit/MetalKit.h>



@interface RootViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate>

@property (weak, nonatomic) IBOutlet MTKView *mtkView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *imageViewControllerContainerView;
@property (weak, nonatomic) IBOutlet UIView *MTKViewControllerContainerView;

@end


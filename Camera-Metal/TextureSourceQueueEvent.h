//
//  DispatchTest.h
//  DispatchSourceQueueEventHandlerSampleCode
//
//  Created by Xcode Developer on 5/30/21.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

NS_ASSUME_NONNULL_BEGIN

static int (^generateRandomInt)(void) = ^int(void) {
    int random_int = arc4random_uniform((uint)time(0));
    return random_int;
};

typedef struct DispatchSourceObjectInt
{
    int rnd_int;
} DispatchSourceObjectInt;

typedef struct{
    void *texture;
} Texture;


//@property (nonatomic, strong) __attribute__((NSObject)) CVPixelBufferRef pixelBuffer;
//typedef __attribute__((NSObject)) CVPixelBufferRef pixelBuffer;
typedef struct
{
    __unsafe_unretained id<MTLTexture>(^CVPixelBufferCreateMTLTexture)(void);
} CVPixelBufferCreateMTLTextureContext;

static DispatchSourceObjectInt * (^randomInt)(void) = ^DispatchSourceObjectInt *(void) {
    struct DispatchSourceObjectInt * dispatch_source_object_int_ptr = malloc(sizeof(struct DispatchSourceObjectInt));
    dispatch_source_object_int_ptr->rnd_int = generateRandomInt();
    return dispatch_source_object_int_ptr;
};

@interface TextureSourceQueueEvent : NSObject

+ (nonnull TextureSourceQueueEvent *)textureSourceQueueEvent;

@property (nonatomic) __block long event_index;
//@property (strong, nonatomic) __block dispatch_queue_t textureDispatchQueue;
//@property (strong, nonatomic, nullable) __block dispatch_source_t textureDispatchSource;

@end


NS_ASSUME_NONNULL_END

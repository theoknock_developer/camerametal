//
//  ViewController.m
//  Camera-Metal
//
//  Created by Xcode Developer on 5/29/21.
//

#import "RootViewController.h"
#import "Camera.h"
#import "Metal/Metal.h"
#import "MetalKit/MetalKit.h"

#import "ImageViewController.h"
#import "MetalViewController.h"

#import "TextureSourceQueueEvent.h"
#import "SharedTypes.h"

@interface RootViewController ()

@end

@implementation RootViewController

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)viewDidLoad {
    NSLog(@"%s", __PRETTY_FUNCTION__);
    
    [super viewDidLoad];
    
    [[Camera sharedCamera] sharedCaptureDeviceWithVideoOutputDelegate:(id<AVCaptureVideoDataOutputSampleBufferDelegate>)self];
}

// TO-DO: Instead of passing two pixel buffers, create a texture and then use CVMetalTextureCacheCreateTextureFromImage to copy the texture
// and send to the recipients
- (void)captureOutput:(AVCaptureOutput *)output didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    CVPixelBufferRef pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    PixelBufferData * pixel_buffer_data = PixelBufferDataRefForPlane(pixelBuffer, 0);
    const char * pixel_buffer_data_context_label = [[NSString stringWithFormat:@"%d", 1] cStringUsingEncoding:NSUTF8StringEncoding];
    dispatch_queue_set_specific(((ImageViewController *)[self.childViewControllers objectAtIndex:0]).CVPixelBufferDispatchQueue, pixel_buffer_data_context_label, pixel_buffer_data, NULL);
    dispatch_source_merge_data(((ImageViewController *)[self.childViewControllers objectAtIndex:0]).CVPixelBufferDispatchSource, 1);
    
    PixelBufferData * pixel_buffer_data_1 = PixelBufferDataRefForPlane(pixelBuffer, 0);
    const char * pixel_buffer_data_context_label_1 = [[NSString stringWithFormat:@"%d", 1] cStringUsingEncoding:NSUTF8StringEncoding];
    dispatch_queue_set_specific(((MetalViewController *)[self.childViewControllers objectAtIndex:1]).CVPixelBufferDispatchQueue, pixel_buffer_data_context_label_1, pixel_buffer_data_1, NULL);
    dispatch_source_merge_data(((MetalViewController *)[self.childViewControllers objectAtIndex:1]).CVPixelBufferDispatchSource, 1);
    
//    PixelBufferData *metal_view_context = PixelBufferDataRef(pixelBuffer);
//    const char *metal_view_context_label = [[NSString stringWithFormat:@"%d", 1] cStringUsingEncoding:NSUTF8StringEncoding];
//    dispatch_queue_set_specific(((MetalViewController *)[self.childViewControllers objectAtIndex:1]).CVPixelBufferDispatchQueue, metal_view_context_label, PixelBufferDataRefForPlane(pixelBuffer, 0), NULL);
//    dispatch_source_merge_data(((MetalViewController *)[self.childViewControllers objectAtIndex:1]).CVPixelBufferDispatchSource, 1);
//    
//    PixelBufferData * cancel(((MTKViewController *)[self.childViewControllers objectAtIndex:1]).CVPixelBufferDispatchSource, self->_textureHandler.event_index);
}

- (void)captureOutput:(AVCaptureOutput *)output didDropSampleBuffer:(CMSampleBufferRef)sampleBuffer fromConnection:(AVCaptureConnection *)connection {
    NSLog(@"%s", __PRETTY_FUNCTION__);
}

@end

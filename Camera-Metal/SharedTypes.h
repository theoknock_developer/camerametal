//
//  SharedTypes.h
//  Camera-Metal
//
//  Created by Xcode Developer on 6/3/21.
//

#ifndef SharedTypes_h
#define SharedTypes_h

#import <CoreMedia/CoreMedia.h>
#import <Metal/Metal.h>

typedef struct PixelBufferData {
    void * base_address;
    size_t width;
    size_t height;
    size_t bytes_per_row;
    OSType pixel_format_type;
} PixelBufferData;

static PixelBufferData * (^PixelBufferDataRefForPlane)(CVPixelBufferRef, size_t) = ^ PixelBufferData * (CVPixelBufferRef pixel_buffer, size_t planeIndex) {
    CVPixelBufferLockBaseAddress(pixel_buffer, kCVPixelBufferLock_ReadOnly);
    PixelBufferData *context    = (PixelBufferData *)malloc(sizeof(PixelBufferData));
    context->base_address       = CVPixelBufferGetBaseAddressOfPlane(pixel_buffer, planeIndex);
    context->width              = CVPixelBufferGetWidthOfPlane(pixel_buffer, planeIndex);
    context->height             = CVPixelBufferGetHeightOfPlane(pixel_buffer, planeIndex);
    context->bytes_per_row      = CVPixelBufferGetBytesPerRowOfPlane(pixel_buffer, planeIndex);
    context->pixel_format_type  = CVPixelBufferGetPixelFormatType(pixel_buffer);
    
    CVPixelBufferUnlockBaseAddress(pixel_buffer, kCVPixelBufferLock_ReadOnly);
    
    return context;
};

static void * DispatchEventTypePixelBufferDataKey = &DispatchEventTypePixelBufferDataKey;

typedef NS_ENUM(uintptr_t, DispatchEventDataType) {
    DispatchEventDataTypePixelBufferData
};

static id<MTLTexture> (^CVPixelBufferCreateMTLTexture)(PixelBufferData *, id<MTLDevice>) = ^ id<MTLTexture> (PixelBufferData * pixel_buffer_data, id<MTLDevice> device) {
    MTLTextureDescriptor *textureDescriptor = [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:MTLPixelFormatBGRA8Unorm width:pixel_buffer_data->width height:pixel_buffer_data->height mipmapped:NO];
    id<MTLTexture> texture = [device newTextureWithDescriptor:textureDescriptor];
    MTLRegion region = {
        { 0, 0, 0 },
        {pixel_buffer_data->width, pixel_buffer_data->height, 1}
    };
    
    [texture replaceRegion:region
               mipmapLevel:0
                 withBytes:pixel_buffer_data->base_address
               bytesPerRow:pixel_buffer_data->bytes_per_row];
    
    return texture;
};

#endif /* SharedTypes_h */

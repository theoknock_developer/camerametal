//
//  MTKViewController.m
//  Camera-Metal
//
//  Created by Xcode Developer on 5/31/21.
//

#import "MTKViewController.h"

static const NSUInteger MaxBuffersInFlight = 3;

@interface MTKViewController () <MTKViewDelegate>

@end

@implementation MTKViewController
//{
//    id<MTLDevice> _device;
//    MTLVertexDescriptor *_mtlVertexDescriptor;
//    id <MTLBuffer> _dynamicUniformBuffer[MaxBuffersInFlight];
//    id<MTLLibrary> _library;
//    id<MTLRenderPipelineState> _pipelineState;
//    id <MTLDepthStencilState> _depthState;
//    id<MTLBuffer> _vertexBuffer;
//    id<MTLBuffer> _uniformBuffer;
//    id<MTLCommandQueue> _commandQueue;
//    CVMetalTextureCacheRef _textureCache;
//    
//    NSArray <CAMetalLayer *> *metalLayers;
//    id <CAMetalDrawable> _currentDrawable[2];
//    BOOL _layerSizeDidUpdate;
//}
//
//- (void)viewDidLoad {
//    [super viewDidLoad];
//    // Do any additional setup after loading the view.
//    
//    self->_textureHandler = [TextureSourceQueueEvent textureSourceQueueEvent];
//    
//    CAMetalLayer *metalLayer = (CAMetalLayer *)((MTKView *)self.view).layer;
//    [metalLayer setBackgroundColor:[UIColor grayColor].CGColor];
//    metalLayer.contentsGravity = kCAGravityResizeAspectFill;
//    metalLayer.contentsScale= [[UIScreen mainScreen] nativeScale];
//    metalLayer.pixelFormat = MTLPixelFormatBGRA8Unorm;
//    // Change this to NO if the compute encoder is used as the last pass on the drawable texture
//    metalLayer.framebufferOnly = YES;
//    
//    _device = MTLCreateSystemDefaultDevice();
//    ((MTKView *)self.view).device = _device;
//    CVMetalTextureCacheCreate(NULL, NULL, ((MTKView *)self.view).device, NULL, &_textureCache); // Try removing once everything is working; this may be unnecessary
//    
//    ((MTKView *)self.view).depthStencilPixelFormat = MTLPixelFormatDepth32Float_Stencil8;
//    ((MTKView *)self.view).colorPixelFormat = MTLPixelFormatBGRA8Unorm;
//    ((MTKView *)self.view).sampleCount = 1;
//    
//    _mtlVertexDescriptor = [[MTLVertexDescriptor alloc] init];
//    
//    _mtlVertexDescriptor.attributes[VertexAttributePosition].format = MTLVertexFormatFloat3;
//    _mtlVertexDescriptor.attributes[VertexAttributePosition].offset = 0;
//    _mtlVertexDescriptor.attributes[VertexAttributePosition].bufferIndex = BufferIndexMeshPositions;
//    
//    _mtlVertexDescriptor.attributes[VertexAttributeTexcoord].format = MTLVertexFormatFloat2;
//    _mtlVertexDescriptor.attributes[VertexAttributeTexcoord].offset = 0;
//    _mtlVertexDescriptor.attributes[VertexAttributeTexcoord].bufferIndex = BufferIndexMeshGenerics;
//    
//    _mtlVertexDescriptor.layouts[BufferIndexMeshPositions].stride = 12;
//    _mtlVertexDescriptor.layouts[BufferIndexMeshPositions].stepRate = 1;
//    _mtlVertexDescriptor.layouts[BufferIndexMeshPositions].stepFunction = MTLVertexStepFunctionPerVertex;
//    
//    _mtlVertexDescriptor.layouts[BufferIndexMeshGenerics].stride = 8;
//    _mtlVertexDescriptor.layouts[BufferIndexMeshGenerics].stepRate = 1;
//    _mtlVertexDescriptor.layouts[BufferIndexMeshGenerics].stepFunction = MTLVertexStepFunctionPerVertex;
//    
//    id<MTLLibrary> defaultLibrary = [_device newDefaultLibrary];
//    
//    id <MTLFunction> vertexFunction = [defaultLibrary newFunctionWithName:@"vertexShader"];
//    
//    id <MTLFunction> fragmentFunction = [defaultLibrary newFunctionWithName:@"fragmentShader"];
//    
//    MTLRenderPipelineDescriptor *pipelineStateDescriptor = [[MTLRenderPipelineDescriptor alloc] init];
//    pipelineStateDescriptor.label = @"MyPipeline";
//    pipelineStateDescriptor.sampleCount = ((MTKView *)self.view).sampleCount;
//    pipelineStateDescriptor.vertexFunction = vertexFunction;
//    pipelineStateDescriptor.fragmentFunction = fragmentFunction;
//    pipelineStateDescriptor.vertexDescriptor = _mtlVertexDescriptor;
//    pipelineStateDescriptor.colorAttachments[0].pixelFormat = ((MTKView *)self.view).colorPixelFormat;
//    pipelineStateDescriptor.depthAttachmentPixelFormat = ((MTKView *)self.view).depthStencilPixelFormat;
//    pipelineStateDescriptor.stencilAttachmentPixelFormat = ((MTKView *)self.view).depthStencilPixelFormat;
//    
//    NSError *error = NULL;
//    _pipelineState = [_device newRenderPipelineStateWithDescriptor:pipelineStateDescriptor error:&error];
//    if (!_pipelineState)
//    {
//        NSLog(@"Failed to created pipeline state, error %@", error);
//    }
//    
//    MTLDepthStencilDescriptor *depthStateDesc = [[MTLDepthStencilDescriptor alloc] init];
//    depthStateDesc.depthCompareFunction = MTLCompareFunctionLess;
//    depthStateDesc.depthWriteEnabled = YES;
//    _depthState = [_device newDepthStencilStateWithDescriptor:depthStateDesc];
//    
//    for(NSUInteger i = 0; i < MaxBuffersInFlight; i++)
//    {
//        _dynamicUniformBuffer[i] = [_device newBufferWithLength:sizeof(Uniforms)
//                                                        options:MTLResourceStorageModeShared];
//        
//        _dynamicUniformBuffer[i].label = @"UniformBuffer";
//    }
//    
//    _commandQueue = [_device newCommandQueue];
//    
//    [(MTKView *)self.view setDelegate:(id<MTKViewDelegate> _Nullable)self];
//    
//    [((MTKView *)self.view).delegate mtkView:(MTKView *)self.view drawableSizeWillChange:self.parentViewController.view.bounds.size];
//}
//
//
//MTLRenderPassDescriptor *(^setupRenderPassDescriptorForTexture)(id<MTLTexture>) = ^(id<MTLTexture> texture) {
//    printf("%s\n", __PRETTY_FUNCTION__);
//    MTLRenderPassDescriptor *renderPassDescriptor = [MTLRenderPassDescriptor renderPassDescriptor];
//    
//    renderPassDescriptor.colorAttachments[0].texture = texture;
//    renderPassDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
//    renderPassDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(0.65f, 0.65f, 0.65f, 1.0f);
//    renderPassDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
//    
//    return renderPassDescriptor;
//};
//
//void(^render)(id<MTLCommandBuffer>, id <MTLDepthStencilState>, id<MTLRenderPipelineState>, id<MTLBuffer>,  id<MTLBuffer>, id<CAMetalDrawable>, id<MTLTexture>) = ^(id<MTLCommandBuffer> commandBuffer, id <MTLDepthStencilState> depthState, id<MTLRenderPipelineState> pipelineState, id<MTLBuffer> vertexBuffer, id<MTLBuffer> uniformBuffer, id<CAMetalDrawable> drawable, id<MTLTexture> texture) {
//    printf("%s\n", __PRETTY_FUNCTION__);
//    id <MTLRenderCommandEncoder> renderCommandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:setupRenderPassDescriptorForTexture(drawable.texture)];
//    [renderCommandEncoder setDepthStencilState:depthState];
//    
//    // Set context state
//    if(texture != nil)
//    {
//        [renderCommandEncoder pushDebugGroup:@"DrawTexture"];
//        [renderCommandEncoder setRenderPipelineState:pipelineState];
//        [renderCommandEncoder setVertexBuffer:vertexBuffer offset:0 atIndex:0];
//        [renderCommandEncoder setVertexBuffer:uniformBuffer offset:0 atIndex:1];
//        [renderCommandEncoder setFragmentTexture:texture atIndex:0];
//        
//        // Tell the render context we want to draw our primitives
//        [renderCommandEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4 instanceCount:2];
//        [renderCommandEncoder popDebugGroup];
//    }
//    [renderCommandEncoder endEncoding];
//    
//    [commandBuffer presentDrawable:drawable];
//    [commandBuffer commit];
//};
//
//- (dispatch_queue_t)CVPixelBufferDispatchQueue
//{
//    __block dispatch_queue_t q = self->_CVPixelBufferDispatchQueue;
//    if (!q)
//    {
//        static dispatch_once_t onceToken;
//        dispatch_once(&onceToken, ^{
//            q = dispatch_queue_create_with_target("CVPixelBufferDispatchQueue", DISPATCH_QUEUE_CONCURRENT, dispatch_get_main_queue());
//            self->_CVPixelBufferDispatchQueue = q;
//        });
//    }
//    
//    return q;
//}
//
//- (dispatch_source_t)CVPixelBufferDispatchSource
//{
//    dispatch_queue_t dispatch_queue = self->_CVPixelBufferDispatchQueue;
//    dispatch_source_t dispatch_source = self->_CVPixelBufferDispatchSource;
//    if (!dispatch_source)
//    {
//        dispatch_source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_queue);
//        dispatch_source_set_event_handler(dispatch_source, ^{
//            dispatch_async(dispatch_queue, ^{
//                NSInteger index = dispatch_source_get_data(dispatch_source);
//                PixelBufferData *context = (PixelBufferData *)malloc(sizeof(PixelBufferData));
//                if (context != NULL)
//                {
//                    context = (PixelBufferData *)dispatch_queue_get_specific(dispatch_queue, [[NSString stringWithFormat:@"%lu", index] cStringUsingEncoding:NSUTF8StringEncoding]);
//                    //                    id<MTLTexture> texture = (id<MTLTexture>)CFBridgingRelease(context->texture);
//                    MTLTextureDescriptor *textureDescriptor = [[MTLTextureDescriptor alloc] init];
//                    textureDescriptor.pixelFormat = MTLPixelFormatBGRA8Unorm;
//                    textureDescriptor.width = context->width;
//                    textureDescriptor.height = context->height;
//                    id<MTLTexture> texture = [((MTKView *)self.view).device newTextureWithDescriptor:textureDescriptor];
//                    MTLRegion region = {
//                        { 0, 0, 0 },
//                        {context->width, context->height, 1}
//                    };
//                    NSUInteger bytesPerRow = 4 * context->width;
//                    [texture replaceRegion:region
//                               mipmapLevel:0
//                                 withBytes:context->pixelBufferData
//                               bytesPerRow:bytesPerRow];
//            
////                    NSLog(@"MTKView width\t%lu\t\theight\t%lu", texture.width, texture.height);
//                }
//                
//                
//            });
//        });
//        dispatch_set_target_queue(dispatch_source, dispatch_queue);
//        dispatch_resume(dispatch_source);
//        self->_CVPixelBufferDispatchSource = dispatch_source;
//    }
//    return self->_CVPixelBufferDispatchSource;
//}
//
////- (dispatch_source_t)CVPixelBufferDispatchSource
////{
////    dispatch_queue_t dispatch_queue = self->_textureHandler.textureDispatchQueue;
////    dispatch_source_t dispatch_source = self->_CVPixelBufferDispatchSource; // consider NULLing the dispatch source here instead of inside the video capture delegate method
////    if (!dispatch_source)
////    {
////        dispatch_source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_queue);
////        dispatch_source_set_event_handler(dispatch_source, ^{
////            dispatch_async(dispatch_queue, ^{
////                NSInteger index = dispatch_source_get_data(dispatch_source);
////
////                CVPixelBufferCreateMTLTextureContext *context = (CVPixelBufferCreateMTLTextureContext *)malloc(sizeof(CVPixelBufferCreateMTLTextureContext));
////                if (context != NULL)
////                {
////                    context = (CVPixelBufferCreateMTLTextureContext *)dispatch_queue_get_specific(dispatch_queue, [[NSString stringWithFormat:@"%lu", index] cStringUsingEncoding:NSUTF8StringEncoding]);
////                     dispatch_async(dispatch_get_main_queue(), ^{
////                        render([self->_commandQueue commandBuffer],
////                               self->_depthState,
////                               self->_pipelineState,
////                               self->_vertexBuffer,
////                               self->_uniformBuffer,
////                               [(CAMetalLayer *)((MTKView *)self.view).layer nextDrawable],
////                               context->CVPixelBufferCreateMTLTexture());
////                    });
////                }
////            });
////        });
////
////        dispatch_set_target_queue(dispatch_source, dispatch_queue);
////        dispatch_resume(dispatch_source);
////        self->_CVPixelBufferDispatchSource = dispatch_source;
////    }
////    return self->_CVPixelBufferDispatchSource;
////}
//
//#pragma mark Utilities
//
//- (id <CAMetalDrawable>)currentDrawable:(NSInteger)index
//{
//    while (_currentDrawable[index] == nil)
//    {
//        _currentDrawable[index] = [(CAMetalLayer *)((MTKView *)self.view).layer nextDrawable];
//        if (!_currentDrawable[index])
//        {
//            NSLog(@"CurrentDrawable[%lu] is nil", index);
//        }
//    }
//    
//    return _currentDrawable[index];
//}
//
//- (void)drawInMTKView:(MTKView *)view
//{
//    //    id<CAMetalDrawable> drawable = [(CAMetalLayer *)view.layer nextDrawable];
//    //    if (drawable != nil && self.texture != nil) {
//    //        MTLRenderPassDescriptor *passDescriptor = [view currentRenderPassDescriptor];
//    //        passDescriptor.colorAttachments[0].clearColor = MTLClearColorMake(1, 1, 1, 1);
//    //        passDescriptor.colorAttachments[0].loadAction = MTLLoadActionClear;
//    //        passDescriptor.colorAttachments[0].storeAction = MTLStoreActionStore;
//    //        passDescriptor.colorAttachments[0].texture = [drawable texture];
//    //        if (([[DisplayConfigurationManager sharedDisplayConfigurationManager] globalConfiguration] % 2) ||
//    //            ([[DisplayConfigurationManager sharedDisplayConfigurationManager] globalConfiguration] == 0))
//    //        {
//    //            passDescriptor.renderTargetWidth = self.texture.height;
//    //            passDescriptor.renderTargetHeight = self.texture.width;
//    //        } else {
//    //            passDescriptor.renderTargetWidth = self.texture.width;
//    //            passDescriptor.renderTargetHeight = self.texture.height;
//    //        }
//    //
//    //        if (passDescriptor)
//    //        {
//    //        id<MTLCommandBuffer> commandBuffer = [self.metalContext.commandQueue commandBuffer];
//    //
//    //        id<MTLRenderCommandEncoder> commandEncoder = [commandBuffer renderCommandEncoderWithDescriptor:passDescriptor];
//    //        if (self.texture != nil) {
//    //            [commandEncoder pushDebugGroup:@"commandEncoder debug group"];
//    //            [commandEncoder setRenderPipelineState:self.metalContext.renderPipelineState];
//    //            [commandEncoder setVertexBuffer:self.metalContext.vertexBuffer offset:0 atIndex:0];
//    //            // TO-DO: the scale doesn't need to be called every draw call; set in gesture handler
//    //            [commandEncoder setVertexBuffer:self.metalContext.uniformBuffer offset:0 atIndex:1];
//    //
//    //            [commandEncoder setFragmentSamplerState:self.metalContext.samplerState atIndex:0];
//    //            [commandEncoder setFragmentBuffer:self.metalContext.uniformBuffer offset:0 atIndex:1];
//    //
//    //            if (view.tag != 3)
//    //            {
//    //                [self.metalContext updateScaleFactor:1.0];
//    //            } else {
//    //                [self.metalContext updateScaleFactor:[[DisplayConfigurationManager sharedDisplayConfigurationManager] zoomScale]];
//    //            }
//    //
//    //            [commandEncoder setFragmentTexture:self.texture atIndex:0];
//    //            [commandEncoder drawPrimitives:MTLPrimitiveTypeTriangleStrip vertexStart:0 vertexCount:4];
//    //            [commandEncoder popDebugGroup];
//    //        }
//    //        [commandEncoder endEncoding];
//    //
//    //        [commandBuffer presentDrawable:drawable];
//    //            [commandBuffer addCompletedHandler:^(id<MTLCommandBuffer> _Nonnull) {
//    ////                dispatch_async(dispatch_get_main_queue(), ^{
//    ////                    [_zoomView1 setNeedsDisplay];
//    ////                    [_zoomView2 setNeedsDisplay];
//    ////                });
//    //            }];
//    //        [commandBuffer commit];
//    //        }
//    //    }
//}
//
//- (void)mtkView:(MTKView *)view drawableSizeWillChange:(CGSize)size
//{
//    //    CGRect frame = CGRectMake(self.view.superview.frame.origin.x, self.view.superview.frame.origin.y, size.width, size.height);
//    //    [view setFrame:frame];
//}


@end

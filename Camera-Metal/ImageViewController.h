//
//  ImageViewController.h
//  Camera-Metal
//
//  Created by Xcode Developer on 5/31/21.
//

#import <UIKit/UIKit.h>

#import "TextureSourceQueueEvent.h"

NS_ASSUME_NONNULL_BEGIN

@interface ImageViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic, nullable) __block dispatch_source_t CVPixelBufferDispatchSource;
@property (strong, nonatomic, nullable) __block dispatch_queue_t CVPixelBufferDispatchQueue;
@property (strong, nonatomic) __block TextureSourceQueueEvent *textureHandler;
@end

NS_ASSUME_NONNULL_END

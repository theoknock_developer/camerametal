//
//  ImageViewController.m
//  Camera-Metal
//
//  Created by Xcode Developer on 5/31/21.
//

#import "ImageViewController.h"
#import "SharedTypes.h"

#import <Metal/Metal.h>

@interface ImageViewController ()

@end

@implementation ImageViewController
{
    CGColorSpaceRef _colorSpace;
    id<MTLDevice> _device;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _colorSpace =  CGColorSpaceCreateDeviceRGB();
    _device = MTLCreateSystemDefaultDevice();

    self->_textureHandler = [TextureSourceQueueEvent textureSourceQueueEvent];
}

//- (dispatch_source_t)UIImageDispatchSource
//{
//    dispatch_queue_t dispatch_queue = self->_textureHandler.textureDispatchQueue;
//    dispatch_source_t dispatch_source = self->_UIImageDispatchSource;
//    if (!dispatch_source)
//    {
//        dispatch_source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_queue);
//        dispatch_source_set_event_handler(dispatch_source, ^{
//            dispatch_async(dispatch_queue, ^{
//                NSInteger index = dispatch_source_get_data(dispatch_source);
//                Texture *context = (Texture *)malloc(sizeof(Texture));
//                if (context != NULL)
//                {
//
//                    context = (Texture *)dispatch_queue_get_specific(dispatch_queue, [[NSString stringWithFormat:@"%lu", index] cStringUsingEncoding:NSUTF8StringEncoding]);
//                    id<MTLTexture> texture = (id<MTLTexture>)CFBridgingRelease(context->texture);
//                    dispatch_async(dispatch_get_main_queue(), ^{
//                        CIImage *image = [[[CIImage alloc] initWithMTLTexture:texture options:@{kCIImageColorSpace : CFBridgingRelease(_colorSpace)}] imageByApplyingCGOrientation:kCGImagePropertyOrientationLeftMirrored];
//                        UIImage *uiimage = [UIImage imageWithCIImage:image];
//                        [(UIImageView *)self.view setImage:uiimage];
//                    });
//                }
//
//
//            });
//        });
//        dispatch_set_target_queue(dispatch_source, dispatch_queue);
//
//
//        self->_UIImageDispatchSource = dispatch_source;
//        dispatch_resume(self->_UIImageDispatchSource);
//    }
//
//    return self->_UIImageDispatchSource;
//}

- (dispatch_queue_t)CVPixelBufferDispatchQueue
{
    __block dispatch_queue_t q = self->_CVPixelBufferDispatchQueue;
    if (!q)
    {
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            q = dispatch_queue_create_with_target("CVPixelBufferDispatchQueue", DISPATCH_QUEUE_CONCURRENT, dispatch_get_main_queue());
            self->_CVPixelBufferDispatchQueue = q;
        });
    }
    
    return q;
}

- (dispatch_source_t)CVPixelBufferDispatchSource
{
    dispatch_queue_t dispatch_queue = self->_CVPixelBufferDispatchQueue;
    dispatch_source_t dispatch_source = self->_CVPixelBufferDispatchSource;
    if (!dispatch_source)
    {
        dispatch_source = dispatch_source_create(DISPATCH_SOURCE_TYPE_DATA_ADD, 0, 0, dispatch_queue);
        dispatch_source_set_event_handler(dispatch_source, ^{
            dispatch_async(dispatch_queue, ^{
                id<MTLTexture> texture = CVPixelBufferCreateMTLTexture((PixelBufferData *)dispatch_queue_get_specific(dispatch_queue, [[NSString stringWithFormat:@"%lu", dispatch_source_get_data(dispatch_source)] cStringUsingEncoding:NSUTF8StringEncoding]), self->_device);
                    CIImage *image = [[[CIImage alloc] initWithMTLTexture:texture options:@{kCIImageColorSpace : CFBridgingRelease(self->_colorSpace)}] imageByApplyingCGOrientation:kCGImagePropertyOrientationLeftMirrored];
                    UIImage *uiimage = [UIImage imageWithCIImage:image];
                    [(UIImageView *)self.imageView setImage:uiimage];
            });
        });
        dispatch_set_target_queue(dispatch_source, dispatch_queue);
        dispatch_resume(dispatch_source);
        self->_CVPixelBufferDispatchSource = dispatch_source;
    }
    return self->_CVPixelBufferDispatchSource;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
